from django.conf.urls import url
from django.contrib import admin
from rest_framework.compat import include
from rest_framework.routers import DefaultRouter

from users.views import UserViewSet, UserLoginView, UserLogOutView

router = DefaultRouter()

router.register(r'user', UserViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls, namespace='api')),
    url(r'^api/auth/login/$', UserLoginView.as_view()),
    url(r'^api/auth/logout/$', UserLogOutView.as_view()),
]
