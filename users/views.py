from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import get_user_model

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    model = get_user_model()
    queryset = get_user_model().objects.all()

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()

    def dispatch(self, request, *args, **kwargs):
        print(request.user.is_anonymous())
        if kwargs.get('pk') == 'current' and request.user:
            kwargs['pk'] = request.user.pk

        return super(UserViewSet, self).dispatch(request, *args, **kwargs)


class UserLoginView(APIView):
    def post(self, request):
        user = authenticate(email=request.data.get('email'),
                            password=request.data.get('password'))
        if user is not None:
            login(request, user)
            return Response(UserSerializer(user).data)
        return Response({'error': 'Login error'}, status=400)


class UserLogOutView(APIView):
    def get(self, request):
        logout(request)
        return Response({'status': 'ok'})
