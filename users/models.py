from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)
    type = models.CharField(_('type'), max_length=255, null=True, blank=True)
    username = models.CharField(_('username'), max_length=150, null=True,
                                blank=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['type', 'username']