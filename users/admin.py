from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin


class UsersAdmin(UserAdmin):
    list_display = ('id', 'email', 'type')


admin.site.register(get_user_model(), UsersAdmin)
