from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    type = serializers.CharField(allow_null=False)

    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'password', 'email', 'type')
        write_only_fields = ('password', 'type')
        read_only_fields = ('id',)

    def update(self, instance, validated_data):
        if not self.context['request'].user.is_superuser:
            if instance.is_superuser != validated_data.get('is_superuser') or \
                            instance.is_staff != validated_data.get(
                        'is_staff'):
                raise PermissionDenied()

        return super(UserSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        # call set_password on user object. Without this
        # the password will be stored in plain text.
        if not self.context['request'].user.is_superuser:
            validated_data['is_superuser'] = False
            validated_data['is_staff'] = False
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user
